package com.windx.controller;


import cn.dev33.satoken.stp.StpUtil;
import com.windx.common.ApiResult;
import com.windx.model.dto.CommentDTO;
import com.windx.model.entity.BmsComment;
import com.windx.model.entity.UmsUser;
import com.windx.model.vo.CommentVO;
import com.windx.service.IBmsCommentService;
import com.windx.service.IUmsUserService;
import org.noear.solon.annotation.*;

import java.util.List;

@Controller
@Mapping("/comment")
public class BmsCommentController extends BaseController {

    @Inject
    private IBmsCommentService bmsCommentService;
    @Inject
    private IUmsUserService umsUserService;

    @Get
    @Mapping("/get_comments")
    public ApiResult<List<CommentVO>> getCommentsByTopicID(@Param(value = "topicid", defaultValue = "1") String topicid) {
        List<CommentVO> lstBmsComment = bmsCommentService.getCommentsByTopicID(topicid);
        return ApiResult.success(lstBmsComment);
    }

    @Post
    @Mapping("/add_comment")
    public ApiResult<BmsComment> add_comment(@Body CommentDTO dto) {
        String userName = StpUtil.getLoginIdAsString();
        UmsUser user = umsUserService.getUserByUsername(userName);
        BmsComment comment = bmsCommentService.create(dto, user);
        return ApiResult.success(comment);
    }
}
