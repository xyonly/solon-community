package com.windx.exception;

import com.windx.common.ResultCode;

/**
 * @author 倚栏听风
 * @date 2023/4/16 15:14
 */
public class ApiAsserts {
    /**
     * 抛失败异常
     *
     * @param message 说明
     */
    public static void fail(String message) {
        throw new ApiException(message);
    }

    /**
     * 抛失败异常
     *
     * @param code 状态码
     */
    public static void fail(ResultCode code) {
        throw new ApiException(code);
    }
}
