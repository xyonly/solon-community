package com.windx.service.impl;


import com.baomidou.mybatisplus.solon.service.impl.ServiceImpl;
import com.windx.mapper.BmsCommentMapper;
import com.windx.model.dto.CommentDTO;
import com.windx.model.entity.BmsComment;
import com.windx.model.entity.UmsUser;
import com.windx.model.vo.CommentVO;
import com.windx.service.IBmsCommentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.aspect.annotation.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Slf4j
@Service
public class IBmsCommentServiceImpl extends ServiceImpl<BmsCommentMapper, BmsComment> implements IBmsCommentService {
    @Db
    private BmsCommentMapper bmsCommentMapper;

    @Override
    public List<CommentVO> getCommentsByTopicID(String topicid) {
        List<CommentVO> lstBmsComment = new ArrayList<CommentVO>();
        try {
            lstBmsComment = bmsCommentMapper.getCommentsByTopicID(topicid);
        } catch (Exception e) {
            log.info("lstBmsComment失败");
        }
        return lstBmsComment;
    }

    @Override
    public BmsComment create(CommentDTO dto, UmsUser user) {
        BmsComment comment = BmsComment.builder()
                .userId(user.getId())
                .content(dto.getContent())
                .topicId(dto.getTopic_id())
                .createTime(new Date())
                .build();
        bmsCommentMapper.insert(comment);
        return comment;
    }
}
