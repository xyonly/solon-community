package com.windx.service;


import com.baomidou.mybatisplus.solon.service.IService;
import com.windx.model.dto.CommentDTO;
import com.windx.model.entity.BmsComment;
import com.windx.model.entity.UmsUser;
import com.windx.model.vo.CommentVO;

import java.util.List;

public interface IBmsCommentService extends IService<BmsComment> {
    /**
     * @param topicid
     * @return {@link BmsComment}
     */
    List<CommentVO> getCommentsByTopicID(String topicid);

    BmsComment create(CommentDTO dto, UmsUser principal);
}
