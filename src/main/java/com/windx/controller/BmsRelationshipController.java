package com.windx.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.windx.common.ApiResult;
import com.windx.exception.ApiAsserts;
import com.windx.model.entity.BmsFollow;
import com.windx.model.entity.UmsUser;
import com.windx.service.IBmsFollowService;
import com.windx.service.IUmsUserService;
import org.noear.solon.annotation.*;

import java.util.HashMap;
import java.util.Map;


@Controller
@Mapping("/relationship")
public class BmsRelationshipController extends BaseController {

    @Inject
    private IBmsFollowService bmsFollowService;

    @Inject
    private IUmsUserService umsUserService;

    @Get
    @Mapping("/subscribe/{userId}")
    public ApiResult<Object> handleFollow(@Path("userId") String parentId) {
        String userName = StpUtil.getLoginIdAsString();
        UmsUser umsUser = umsUserService.getUserByUsername(userName);
        if (parentId.equals(umsUser.getId())) {
            ApiAsserts.fail("您脸皮太厚了，怎么可以关注自己呢 😮");
        }
        BmsFollow one = bmsFollowService.getOne(
                new LambdaQueryWrapper<BmsFollow>()
                        .eq(BmsFollow::getParentId, parentId)
                        .eq(BmsFollow::getFollowerId, umsUser.getId()));
        if (!ObjectUtils.isEmpty(one)) {
            ApiAsserts.fail("已关注");
        }

        BmsFollow follow = new BmsFollow();
        follow.setParentId(parentId);
        follow.setFollowerId(umsUser.getId());
        bmsFollowService.save(follow);
        return ApiResult.success(null, "关注成功");
    }

    @Get
    @Mapping("/unsubscribe/{userId}")
    public ApiResult<Object> handleUnFollow(@Path("userId") String parentId) {
        String userName = StpUtil.getLoginIdAsString();
        UmsUser umsUser = umsUserService.getUserByUsername(userName);
        BmsFollow one = bmsFollowService.getOne(
                new LambdaQueryWrapper<BmsFollow>()
                        .eq(BmsFollow::getParentId, parentId)
                        .eq(BmsFollow::getFollowerId, umsUser.getId()));
        if (ObjectUtils.isEmpty(one)) {
            ApiAsserts.fail("未关注！");
        }
        bmsFollowService.remove(new LambdaQueryWrapper<BmsFollow>().eq(BmsFollow::getParentId, parentId)
                .eq(BmsFollow::getFollowerId, umsUser.getId()));
        return ApiResult.success(null, "取关成功");
    }

    @Get
    @Mapping("/validate/{topicUserId}")
    public ApiResult<Map<String, Object>> isFollow(@Path("topicUserId") String topicUserId) {
        String userName = StpUtil.getLoginIdAsString();
        UmsUser umsUser = umsUserService.getUserByUsername(userName);
        Map<String, Object> map = new HashMap<>(16);
        map.put("hasFollow", false);
        if (!ObjectUtils.isEmpty(umsUser)) {
            BmsFollow one = bmsFollowService.getOne(new LambdaQueryWrapper<BmsFollow>()
                    .eq(BmsFollow::getParentId, topicUserId)
                    .eq(BmsFollow::getFollowerId, umsUser.getId()));
            if (!ObjectUtils.isEmpty(one)) {
                map.put("hasFollow", true);
            }
        }
        return ApiResult.success(map);
    }
}
