package com.windx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.windx.model.entity.BmsFollow;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BmsFollowMapper extends BaseMapper<BmsFollow> {
}
