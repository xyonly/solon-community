package com.windx.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.solon.plugins.pagination.Page;
import com.vdurmont.emoji.EmojiParser;
import com.windx.common.ApiResult;
import com.windx.model.dto.CreateTopicDTO;
import com.windx.model.entity.BmsPost;
import com.windx.model.entity.UmsUser;
import com.windx.model.vo.PostVO;
import com.windx.service.IBmsPostService;
import com.windx.service.IUmsUserService;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Validated;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Controller
@Mapping("/post")
public class BmsPostController extends BaseController {

    @Inject
    private IBmsPostService iBmsPostService;
    @Inject
    private IUmsUserService umsUserService;

    @Get
    @Mapping("/list")
    public ApiResult<Page<PostVO>> list(@Param(value = "tab", defaultValue = "latest") String tab,
                                        @Param(value = "pageNo", defaultValue = "1") Integer pageNo,
                                        @Param(value = "size", defaultValue = "10") Integer pageSize) {
        Page<PostVO> list = iBmsPostService.getList(new Page<>(pageNo, pageSize), tab);
        return ApiResult.success(list);
    }

    @Post
    @Mapping("/create")
    public ApiResult<BmsPost> create(@Body CreateTopicDTO dto) {
        String userName = StpUtil.getLoginIdAsString();
        UmsUser user = umsUserService.getUserByUsername(userName);
        BmsPost topic = iBmsPostService.create(dto, user);
        return ApiResult.success(topic);
    }

    @Get
    @Mapping()
    public ApiResult<Map<String, Object>> view(@Param("id") String id) {
        Map<String, Object> map = iBmsPostService.viewTopic(id);
        return ApiResult.success(map);
    }

    @Get
    @Mapping("/recommend")
    public ApiResult<List<BmsPost>> getRecommend(@Param("topicId") String id) {
        List<BmsPost> topics = iBmsPostService.getRecommend(id);
        return ApiResult.success(topics);
    }

    @Post
    @Mapping("/update")
    public ApiResult<BmsPost> update(@Validated @Body BmsPost post) {
        String userName = StpUtil.getLoginIdAsString();
        UmsUser umsUser = umsUserService.getUserByUsername(userName);
        Assert.isTrue(umsUser.getId().equals(post.getUserId()), "非本人无权修改");
        post.setModifyTime(new Date());
        post.setContent(EmojiParser.parseToAliases(post.getContent()));
        iBmsPostService.updateById(post);
        return ApiResult.success(post);
    }

    @Delete
    @Mapping("/delete/{id}")
    public ApiResult<String> delete(@Path("id") String id) {
        String userName = StpUtil.getLoginIdAsString();
        UmsUser umsUser = umsUserService.getUserByUsername(userName);
        BmsPost byId = iBmsPostService.getById(id);
        Assert.notNull(byId, "来晚一步，话题已不存在");
        Assert.isTrue(byId.getUserId().equals(umsUser.getId()), "你为什么可以删除别人的话题？？？");
        iBmsPostService.removeById(id);
        return ApiResult.success(null, "删除成功");
    }

}
