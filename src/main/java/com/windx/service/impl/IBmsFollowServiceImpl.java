package com.windx.service.impl;


import com.baomidou.mybatisplus.solon.service.impl.ServiceImpl;
import com.windx.mapper.BmsFollowMapper;
import com.windx.model.entity.BmsFollow;
import com.windx.service.IBmsFollowService;
import org.noear.solon.aspect.annotation.Service;

@Service
public class IBmsFollowServiceImpl extends ServiceImpl<BmsFollowMapper, BmsFollow> implements IBmsFollowService {
}
