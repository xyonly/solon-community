package com.windx.exception;

import com.windx.common.ResultCode;

/**
 * @author 倚栏听风
 * @date 2023/4/16 15:14
 */
public class ApiException extends RuntimeException {
    private ResultCode code;

    public ApiException(ResultCode code) {
        super(code.getMessage());
        this.code = code;
    }

    public ApiException(String message) {
        super(message);
    }

    public ResultCode getCode() {
        return code;
    }
}
