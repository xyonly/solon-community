package com.windx.model.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class CommentDTO implements Serializable {


    private String topic_id;

    /**
     * 内容
     */
    private String content;



}
