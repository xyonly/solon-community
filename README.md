# Solon-Community

> 本项目源自以下开源项目
>
>  后端使用solon+sa-token框架替换spring boot
>
> https://github.com/songboriceman/doubao_community_frontend 
>
> https://github.com/songboriceman/doubao_community_backend



## 后端

| 框架         | 地址                    | 功能                                                    |
| ------------ | ----------------------- | ------------------------------------------------------- |
| solon        | http://solon.noear.org/ | Web框架                                                 |
| sa-token     | https://sa-token.cc/    | 权限认证框架                                            |
| mybatis-plus | https://baomidou.com/   | [MyBatis](https://www.mybatis.org/mybatis-3/)的增强工具 |

## 前端

Vue

Vuex

Vue Router

Axios

Bulma

Buefy

Element

Vditor

DarkReader