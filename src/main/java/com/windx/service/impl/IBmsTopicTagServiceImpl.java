package com.windx.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.solon.service.impl.ServiceImpl;
import com.windx.mapper.BmsTopicTagMapper;
import com.windx.model.entity.BmsTag;
import com.windx.model.entity.BmsTopicTag;
import com.windx.service.IBmsTopicTagService;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.aspect.annotation.Service;
import org.noear.solon.data.annotation.Tran;

import java.util.List;
import java.util.Set;


@Service
public class IBmsTopicTagServiceImpl extends ServiceImpl<BmsTopicTagMapper, BmsTopicTag> implements IBmsTopicTagService {
    @Db
    private BmsTopicTagMapper bmsTopicTagMapper;

    @Tran
    @Override
    public List<BmsTopicTag> selectByTopicId(String topicId) {
        QueryWrapper<BmsTopicTag> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(BmsTopicTag::getTopicId, topicId);
        return bmsTopicTagMapper.selectList(wrapper);
    }

    @Tran
    @Override
    public void createTopicTag(String id, List<BmsTag> tags) {
        // 先删除topicId对应的所有记录
        bmsTopicTagMapper.delete(new LambdaQueryWrapper<BmsTopicTag>().eq(BmsTopicTag::getTopicId, id));

        // 循环保存对应关联
        tags.forEach(tag -> {
            BmsTopicTag topicTag = new BmsTopicTag();
            topicTag.setTopicId(id);
            topicTag.setTagId(tag.getId());
            bmsTopicTagMapper.insert(topicTag);
        });
    }

    @Tran
    @Override
    public Set<String> selectTopicIdsByTagId(String id) {
        return bmsTopicTagMapper.getTopicIdsByTagId(id);
    }

}
