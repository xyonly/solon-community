package com.windx.common;

/**
 * @author 倚栏听风
 * @date 2023/4/16 15:15
 */
public interface ResultCode {
    /**
     * 错误编码: -1失败;200成功
     *
     * @return 错误编码
     */
    Integer getCode();

    /**
     * 错误描述
     *
     * @return 错误描述
     */
    String getMessage();
}
