package com.windx.service.impl;


import com.baomidou.mybatisplus.solon.service.impl.ServiceImpl;
import com.windx.mapper.BmsPromotionMapper;
import com.windx.model.entity.BmsPromotion;
import com.windx.service.IBmsPromotionService;
import org.noear.solon.aspect.annotation.Service;

@Service
public class IBmsPromotionServiceImpl extends ServiceImpl<BmsPromotionMapper, BmsPromotion> implements IBmsPromotionService {

}
