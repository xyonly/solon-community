package com.windx.config;

import com.zaxxer.hikari.HikariDataSource;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import javax.sql.DataSource;

/**
 * @author 倚栏听风
 * @date 2023/4/16 15:05
 */
@Configuration
public class MybatisPlusConfig {
    @Bean(value = "db", typed = true)
    public DataSource dataSource(@Inject("${mysql.db}") HikariDataSource ds) {
        return ds;
    }
}
