package com.windx.service;


import com.baomidou.mybatisplus.solon.service.IService;
import com.windx.model.entity.BmsBillboard;

public interface IBmsBillboardService extends IService<BmsBillboard> {
}
