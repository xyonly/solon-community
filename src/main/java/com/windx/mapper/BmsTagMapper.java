package com.windx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.windx.model.entity.BmsTag;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BmsTagMapper extends BaseMapper<BmsTag> {

}
