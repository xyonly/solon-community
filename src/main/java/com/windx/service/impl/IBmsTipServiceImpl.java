package com.windx.service.impl;


import com.baomidou.mybatisplus.solon.service.impl.ServiceImpl;
import com.windx.mapper.BmsTipMapper;
import com.windx.model.entity.BmsTip;
import com.windx.service.IBmsTipService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.solon.annotation.Db;
import org.noear.solon.aspect.annotation.Service;


@Slf4j
@Service
public class IBmsTipServiceImpl extends ServiceImpl<BmsTipMapper
        , BmsTip> implements IBmsTipService {

    @Db
    private BmsTipMapper bmsTipMapper;

    @Override
    public BmsTip getRandomTip() {
        BmsTip todayTip = null;
        try {
            todayTip = bmsTipMapper.getRandomTip();
        } catch (Exception e) {
            log.info("tip转化失败");
        }
        return todayTip;
    }
}
