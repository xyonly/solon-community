package com.windx.service.impl;


import com.baomidou.mybatisplus.solon.service.impl.ServiceImpl;
import com.windx.mapper.BmsBillboardMapper;
import com.windx.model.entity.BmsBillboard;
import com.windx.service.IBmsBillboardService;
import org.noear.solon.aspect.annotation.Service;

@Service
public class IBmsBillboardServiceImpl extends ServiceImpl<BmsBillboardMapper
        , BmsBillboard> implements IBmsBillboardService {

}
