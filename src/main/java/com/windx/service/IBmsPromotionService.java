package com.windx.service;


import com.baomidou.mybatisplus.solon.service.IService;
import com.windx.model.entity.BmsPromotion;

public interface IBmsPromotionService extends IService<BmsPromotion> {
}
