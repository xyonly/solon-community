package com.windx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.windx.model.entity.BmsPromotion;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BmsPromotionMapper extends BaseMapper<BmsPromotion> {
}
