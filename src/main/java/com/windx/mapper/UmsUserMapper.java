package com.windx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.windx.model.entity.UmsUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户
 *
 * @author Knox 2020/11/7
 */
@Mapper
public interface UmsUserMapper extends BaseMapper<UmsUser> {

}
