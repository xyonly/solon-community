package com.windx;

import org.noear.solon.Solon;
import org.noear.solon.web.cors.CrossFilter;

/**
 * @author 倚栏听风
 * @date 2023/4/16 14:46
 */
public class App {
    public static void main(String[] args) {
        Solon.start(App.class, args, app -> {
            app.filter(-1, new CrossFilter().allowedOrigins("*"));
        });
    }

}