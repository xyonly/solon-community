package com.windx.service;


import com.baomidou.mybatisplus.solon.service.IService;
import com.windx.model.entity.BmsTip;

public interface IBmsTipService extends IService<BmsTip> {
    BmsTip getRandomTip();
}
