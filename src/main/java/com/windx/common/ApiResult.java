package com.windx.common;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

/**
 * @author 倚栏听风
 * @date 2023/4/16 15:17
 */
@Data
@NoArgsConstructor
public class ApiResult<T> {
    /**
     * 业务状态码
     */
    private long code;
    /**
     * 结果集
     */
    private T data;
    /**
     * 接口描述
     */
    private String message;

    /**
     * 全参
     *
     * @param code    业务状态码
     * @param message 描述
     * @param data    结果集
     */
    public ApiResult(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public ApiResult(ResultCode errorCode) {
        errorCode = Optional.ofNullable(errorCode).orElse(ApiResultCode.FAILED);
        this.code = errorCode.getCode();
        this.message = errorCode.getMessage();
    }

    /**
     * 成功
     *
     * @return {code:200,message:操作成功,data:自定义}
     */
    public static <T> ApiResult<T> success() {
        return new ApiResult<T>(ApiResultCode.SUCCESS.getCode(), ApiResultCode.SUCCESS.getMessage(), null);
    }

    /**
     * 成功
     *
     * @param data 结果集
     * @return {code:200,message:操作成功,data:自定义}
     */
    public static <T> ApiResult<T> success(T data) {
        return new ApiResult<T>(ApiResultCode.SUCCESS.getCode(), ApiResultCode.SUCCESS.getMessage(), data);
    }

    /**
     * 成功
     *
     * @param data    结果集
     * @param message 自定义提示信息
     * @return {code:200,message:自定义,data:自定义}
     */
    public static <T> ApiResult<T> success(T data, String message) {
        return new ApiResult<T>(ApiResultCode.SUCCESS.getCode(), message, data);
    }

    /**
     * 失败返回结果
     */
    public static <T> ApiResult<T> failed() {
        return failed(ApiResultCode.FAILED);
    }

    /**
     * 失败返回结果
     *
     * @param message 提示信息
     * @return {code:枚举ApiResultCode取,message:自定义,data:null}
     */
    public static <T> ApiResult<T> failed(String message) {
        return new ApiResult<T>(ApiResultCode.FAILED.getCode(), message, null);
    }

    /**
     * 失败
     *
     * @param errorCode 错误码
     * @return {code:封装接口取,message:封装接口取,data:null}
     */
    public static <T> ApiResult<T> failed(ResultCode errorCode) {
        return new ApiResult<T>(errorCode.getCode(), errorCode.getMessage(), null);
    }

    /**
     * 失败返回结果
     *
     * @param errorCode 错误码
     * @param message   错误信息
     * @return {code:枚举ApiResultCode取,message:自定义,data:null}
     */
    public static <T> ApiResult<T> failed(ResultCode errorCode, String message) {
        return new ApiResult<T>(errorCode.getCode(), message, null);
    }

    /**
     * 参数验证失败返回结果
     */
    public static <T> ApiResult<T> validateFailed() {
        return failed(ApiResultCode.VALIDATE_FAILED);
    }

    /**
     * 参数验证失败返回结果
     *
     * @param message 提示信息
     */
    public static <T> ApiResult<T> validateFailed(String message) {
        return new ApiResult<T>(ApiResultCode.VALIDATE_FAILED.getCode(), message, null);
    }

    /**
     * 未登录返回结果
     */
    public static <T> ApiResult<T> unauthorized(T data) {
        return new ApiResult<T>(ApiResultCode.UNAUTHORIZED.getCode(), ApiResultCode.UNAUTHORIZED.getMessage(), data);
    }

    /**
     * 未授权返回结果
     */
    public static <T> ApiResult<T> forbidden(T data) {
        return new ApiResult<T>(ApiResultCode.FORBIDDEN.getCode(), ApiResultCode.FORBIDDEN.getMessage(), data);
    }
}
