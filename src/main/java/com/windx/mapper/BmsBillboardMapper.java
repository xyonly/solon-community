package com.windx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.windx.model.entity.BmsBillboard;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BmsBillboardMapper extends BaseMapper<BmsBillboard> {
}
