package com.windx.controller;


import com.windx.common.ApiResult;
import com.windx.model.entity.BmsPromotion;
import com.windx.service.IBmsPromotionService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;

import java.util.List;


@Controller
@Mapping("/promotion")
public class BmsPromotionController extends BaseController {

    @Inject
    private IBmsPromotionService bmsPromotionService;

    @Get
    @Mapping("/all")
    public ApiResult<List<BmsPromotion>> list() {
        List<BmsPromotion> list = bmsPromotionService.list();
        return ApiResult.success(list);
    }

}
