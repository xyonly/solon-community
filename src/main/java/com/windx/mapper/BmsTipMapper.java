package com.windx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.windx.model.entity.BmsTip;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface BmsTipMapper extends BaseMapper<BmsTip> {
    BmsTip getRandomTip();
}
