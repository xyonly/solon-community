package com.windx.controller;


import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.baomidou.mybatisplus.solon.plugins.pagination.Page;
import com.windx.common.ApiResult;
import com.windx.model.dto.LoginDTO;
import com.windx.model.dto.RegisterDTO;
import com.windx.model.entity.BmsPost;
import com.windx.model.entity.UmsUser;
import com.windx.service.IBmsPostService;
import com.windx.service.IUmsUserService;
import org.noear.solon.annotation.*;
import org.noear.solon.validation.annotation.Validated;

import java.util.HashMap;
import java.util.Map;


@Controller
@Mapping("/ums/user")
public class UmsUserController extends BaseController {
    @Inject
    private IUmsUserService iUmsUserService;
    @Inject
    private IBmsPostService iBmsPostService;

    @Post
    @Mapping("/register")
    public ApiResult<Map<String, Object>> register(@Validated @Body RegisterDTO dto) {
        UmsUser user = iUmsUserService.executeRegister(dto);
        if (ObjectUtils.isEmpty(user)) {
            return ApiResult.failed("账号注册失败");
        }
        Map<String, Object> map = new HashMap<>(16);
        map.put("user", user);
        return ApiResult.success(map);
    }

    @Post
    @Mapping("/login")
    public ApiResult<Map<String, String>> login(@Validated @Body LoginDTO dto) {
        String token = iUmsUserService.executeLogin(dto);
        if (ObjectUtils.isEmpty(token)) {
            return ApiResult.failed("账号密码错误");
        }
        Map<String, String> map = new HashMap<>(16);
        map.put("token", token);
        return ApiResult.success(map, "登录成功");
    }

    @Get
    @Mapping("/info")
    public ApiResult<UmsUser> getUser() {
        String userName = StpUtil.getLoginIdAsString();
        UmsUser user = iUmsUserService.getUserByUsername(userName);
        return ApiResult.success(user);
    }

    @Get
    @Mapping("/logout")
    public ApiResult<Object> logOut() {
        String userName = StpUtil.getLoginIdAsString();
        StpUtil.logout(userName);
        return ApiResult.success(null, "注销成功");
    }

    @Get
    @Mapping("/{username}")
    public ApiResult<Map<String, Object>> getUserByName(@Path("username") String username,
                                                        @Param(value = "pageNo", defaultValue = "1") Integer pageNo,
                                                        @Param(value = "size", defaultValue = "10") Integer size) {
        Map<String, Object> map = new HashMap<>(16);
        UmsUser user = iUmsUserService.getUserByUsername(username);
        Assert.notNull(user, "用户不存在");
        Page<BmsPost> page = iBmsPostService.page(new Page<>(pageNo, size),
                new LambdaQueryWrapper<BmsPost>().eq(BmsPost::getUserId, user.getId()));
        map.put("user", user);
        map.put("topics", page);
        return ApiResult.success(map);
    }

    @Post
    @Mapping("/update")
    public ApiResult<UmsUser> updateUser(@Body UmsUser umsUser) {
        iUmsUserService.updateById(umsUser);
        return ApiResult.success(umsUser);
    }
}
