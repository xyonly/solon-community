package com.windx.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.solon.plugins.pagination.Page;
import com.windx.common.ApiResult;
import com.windx.model.entity.BmsPost;
import com.windx.model.entity.BmsTag;
import com.windx.service.IBmsTagService;
import org.noear.solon.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
@Mapping("/tag")
public class BmsTagController extends BaseController {

    @Inject
    private IBmsTagService bmsTagService;

    @Get
    @Mapping("/{name}")
    public ApiResult<Map<String, Object>> getTopicsByTag(
            @Path("name") String tagName,
            @Param(value = "page", defaultValue = "1") Integer page,
            @Param(value = "size", defaultValue = "10") Integer size) {

        Map<String, Object> map = new HashMap<>(16);

        LambdaQueryWrapper<BmsTag> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BmsTag::getName, tagName);
        BmsTag one = bmsTagService.getOne(wrapper);
        Assert.notNull(one, "话题不存在，或已被管理员删除");
        Page<BmsPost> topics = bmsTagService.selectTopicsByTagId(new Page<>(page, size), one.getId());
        // 其他热门标签
        Page<BmsTag> hotTags = bmsTagService.page(new Page<>(1, 10),
                new LambdaQueryWrapper<BmsTag>()
                        .notIn(BmsTag::getName, tagName)
                        .orderByDesc(BmsTag::getTopicCount));

        map.put("topics", topics);
        map.put("hotTags", hotTags);

        return ApiResult.success(map);
    }

}
