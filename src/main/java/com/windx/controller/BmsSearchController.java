package com.windx.controller;


import com.baomidou.mybatisplus.solon.plugins.pagination.Page;
import com.windx.common.ApiResult;
import com.windx.model.vo.PostVO;
import com.windx.service.IBmsPostService;
import org.noear.solon.annotation.*;

@Controller
@Mapping("/search")
public class BmsSearchController extends BaseController {

    @Inject
    private IBmsPostService postService;

    @Get
    @Mapping
    public ApiResult<Page<PostVO>> searchList(@Param("keyword") String keyword,
                                              @Param("pageNum") Integer pageNum,
                                              @Param("pageSize") Integer pageSize) {
        Page<PostVO> results = postService.searchByKey(keyword, new Page<>(pageNum, pageSize));
        return ApiResult.success(results);
    }

}
