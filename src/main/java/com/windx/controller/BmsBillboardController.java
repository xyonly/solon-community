package com.windx.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.windx.common.ApiResult;
import com.windx.model.entity.BmsBillboard;
import com.windx.service.IBmsBillboardService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;

import java.util.List;

@Controller
@Mapping("/billboard")
public class BmsBillboardController extends BaseController {

    @Inject
    private IBmsBillboardService bmsBillboardService;

    @Get
    @Mapping("/show")
    public ApiResult<BmsBillboard> getNotices() {
        List<BmsBillboard> list = bmsBillboardService.list(new
                LambdaQueryWrapper<BmsBillboard>().eq(BmsBillboard::isShow, true));
        return ApiResult.success(list.get(list.size() - 1));
    }
}
