package com.windx.controller;


import com.windx.common.ApiResult;
import com.windx.model.entity.BmsTip;
import com.windx.service.IBmsTipService;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;

@Controller
@Mapping("/tip")
public class BmsTipController extends BaseController {
    @Inject
    private IBmsTipService bmsTipService;

    @Get
    @Mapping("/today")
    public ApiResult<BmsTip> getRandomTip() {
        BmsTip tip = bmsTipService.getRandomTip();
        return ApiResult.success(tip);
    }
}
