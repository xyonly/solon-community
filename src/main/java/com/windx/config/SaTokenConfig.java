package com.windx.config;

import cn.dev33.satoken.jwt.StpLogicJwtForStateless;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.solon.integration.SaTokenInterceptor;
import cn.dev33.satoken.stp.StpLogic;
import cn.dev33.satoken.stp.StpUtil;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 倚栏听风
 * @date 2023/4/16 17:24
 */
@Configuration
public class SaTokenConfig {
    // Sa-Token 整合 jwt (Stateless 无状态模式)
    @Bean
    public StpLogic getStpLogicJwt() {
        return new StpLogicJwtForStateless();
    }

    @Bean
    public SaTokenInterceptor saTokenInterceptor() {
        List<String> urlList = new ArrayList<String>();
        urlList.add("/ums/user/info");
        urlList.add("/ums/user/update");
        urlList.add("/post/create");
        urlList.add("/post/update");
        urlList.add("/post/delete/*");
        urlList.add("/comment/add_comment");
        urlList.add("/relationship/subscribe/*");
        urlList.add("/relationship/unsubscribe/*");
        urlList.add("/relationship/validate/*");
        return new SaTokenInterceptor()
                .addInclude("/**")
                .setAuth(res -> {
                    SaRouter.match(urlList).check(r -> StpUtil.checkLogin());
                });
    }
}
